#### Table of contents:
* [Backslash in Windows & Forward slash in Linux](#backslash-in-windows-and-forward-in-linux)
* [The current working directory(cwd) and change it](#the-current-working-directory-and-change-it)
* [Absolute & Relative path](#absolute-and-relative-path)
* [Create new directory with `os.makedirs()`](#creating-new-directory)
* [The `os.path` module](#the-os-path-module)
    * [Handling Absolute & Relative path](#handling-absolute-path-and-relative-path)
* [Finding File Sizes and Folder Contents](#finding-file-sizes-and-folder-contents)
* [Checking path validity](#checking-path-validity)
* [The File Reading and Writing Process](#the-file-reading-and-writing-process)
    * [Openning Files with `open()`](#openning-files)
    * [Reading the contents of file](#reading-the-contents-of-file)
    * [`readlines()` feature](#readlines-feature)
    * [Writing to Files](#writing-to-files)
* [Saving variables with the shelve module](#saving-variables-with-the-shelve-module)
* [Saving variables with the `pprint.pformat()` function](#saving-variables-with-the-pprint-pformat-function)


`import os` is **required** 

Remember quote (`'`) when enter path

## Backslash in Windows and Forward in Linux 
`os.path.join(...)`
```java
e.g.
>>> os.path.join('usr', 'bin', 'spam
'usr/bin/spam'   # Linux
'isr\\bin\\spam' # Windows
```

## The current working directory and change it
`os.getcwd()`

`os.chdir(<path>)   # Change dir`
```python
>>> os.getcwd()
'/home/b0xf/Git/python-space/...'

>>> os.chdir('/home/b0xf/Git')
'/home/b0xf/Git'
```

## Absolute and Relative path
Absolute : Begins from the root

Relative : Begins from cwd
```python
>>> os.getcwd()
'/usr/bin'   # Absolute path
'./bin'      # Relative path
```

## Creating new directory
`os.makedirs()`

It will create any necessary intermediate folders in order to ensure the full path exists.
```java
os.makedirs('./delicious/waffles)   # will create `delicious`, and `waffles` 
                                    # will be created inside `delicious` 
```

## The os path module

### Handling Absolute and Relative path
`os.path.abspath(<path>)` : return a string of the absolute path.

`os.path.isabs(<path>)` : return **True** if absolute else **False**.

`os.path.relpath(<path>, <start_path>)` : return string of a relative path from the start path to path. If `<start_path>` is not provided, the `cwd` is used as a start path.

**\*Trick:** `os.path.isabs(os.path.abspath(<path>))`

`path = C:\Windows\System32\calc.exe`

basename : `calc.exe`

dirname : `C:\Windows\System32`

\* If we need a path's dir name and base name together, use `os.path.split()`
```java
>>> calcFilePath = 'C:\\Windows\\System32\\calc.exe'
>>> os.path.split(calcFilePath)
('C:\\Windows\\System32', 'calc.exe')
```

\* Or we can just put them in a tuple
```java
>>> (os.path.dirname(calcFilePath), os.path.basename(calcFilePath))
[SIMILAR OUTPUT]
```

\* Return a list of string of each folder use `split()` string method

\*Note: `os.path.sep` is set auto correct folder-separating slash for the right OS

```python
>>> calcFilePath.split(os.path.sep)
['C:', 'Windows', 'System32', 'calc.exe']
```

\* One Linux system, there will be a blank string at the start of returned list:
```python
>>> filePath = '/usr/bin/env/python3'
>>> filePath.split(os.path.sep)
['', 'usr', 'bin', 'env', 'python3']
```

This will work on any OS if you pass it `os.path.sep`

## Finding File Sizes and Folder Contents
`os.path.getsize(<path>)` : return the size in bytes of the file in the path argument

`os.listdir(<path>)` : return a list of filename strings for each file the path argument 

\* Find total size of all the files in a directory
```jave
>>> total_size = 0
>>> for filename in os.listdir('C:\\Windows\\System32'):
        total_size = total_size + os.path.getsize(os.path.join('C:\\Windows\\System32'), filename)
[....]
```

## Checking path validity
`os.path.exists(<path>)` : return **True** if File or Folder exists and else **False**.

`os.path.isfile(<path>)` : return **True** if is File and exists.

`os.path.isdir(<path>)` : return **True** if is Directory and exists.

```java
>>> os.path.exists('/usr/bin')
True
>>> os.path.isfile('/usr/bin')
False
>>> os.path.isdir('/usr/bin')
True
```

\* Tip: We can determine whether there is a DVD or USB Flash Drive currently attached or not.
```java
>>> os.path.exists('D:\\')
False
```

## The File Reading and Writing Process
`open()` : return File Object

`read()` or `write()` : method on File Object

`close()`: close File Object

### Openning Files
`helloFile = open('/home/b0xf/Git/_python-space/automate/hello.txt')`

### Reading the contents of File
```java
>>> helloContent = helloFile.read()
>>> helloContent
'Hello World!'
```

### Readlines feature
`readlines()` : return a **list of string**, one string for each line.
```java
------------   sonnet29.txt   ----------------
When, in disgrace with fortune and men's eyes, 
I all alone beweep my outcast state,
And trouble deaf heaven with my bootless cries,
And look upon myself and curse my fate,
----------------------------------------------
>>> sonnetFile = open('sonnet29.txt')
>>> sonnetFile.readline()
[When, in disgrace with fortune and men's eyes,\n', ' I all ....]
```

### Writing to Files
`'w'` : write mode

`'a'` : append mode

\* If the filename passed to `open()` does not exists, both write and append mode will create new, blank files. 

\*Note: After reading or writing a file, call the `close()` method before opening the file again.
```python
e.g:
>>> baconFile = open('bacon.txt', 'w')
>>> baconFile.write('Hello World!\n')
13
>>> baconFile.close()
```

## Saving variables with the shelve module
Save variables with shelve module
```python
>>> import shelve
>>> shelfFile = shelve.open('mydata')
>>> cats = ['Zophie', 'Pooka', 'Simon']
>>> shelfFile['cats'] = cats
>>> shelfFile.close()

# Re open and retrieve the data from the shelf files. Shelf values dont have to opened in read or write mode - *They can do both once opened*
>>> shelfFile = shelve.open('mydata')
>>> type(shelfFile)
...
>>> shelfFile['cats']
[.....]
>>> shelfFile.close()
```
Just like dictionaries, shelf values have keys() and values() methods that will return **list-like** values of the keys and values in the shelf.

Since these methods return **list-like** values, we should pass it to list() to get them in list form:
```python
>>> shelfFile = shelve.open('mydata')
>>> list(shelfFile.keys())
['cats']
>>> list(shelfFile.values())
[['Zophie', 'Pooka', 'Simon']]
>>> shelfFile.close()
```
\*Note: Plaintext is useful for creating files that you'll read in a text editor, but if you want to save data from your Python programs, use the `shelve` module.

## Saving variables with the pprint pformat function
Recall from Pretty Printing, the `pprint.pformat()` do the same but return this same text as a string instead of printing it. 

Not only is this string formatted to be easy to read, but it is also syntactically correct Python code.

```python
>>> import pprint
>>> cats = [{'name': 'Zophie', 'desc': 'chubby',}, {'name': 'Pooka', 'desc': 'fluffy'}]
>>> pprint.pformat(cats)
"......."
>>> fileObj = open('myCats.py', 'w')
>>> fileObj.write('cats= ' + pprint.pformat(cats) + '\n')
83
>>> fileObj.close()
```

To keep this list in cats available even after we close the shell, we use `pprint.pformat()` to returns it as a string. Once we have the data in cats as a string, it's easy to write the string to a file, which we'll we call `myCats.py`

\* Funfact: The modules that an import statement imports are themselve just Python scripts. When the string from `pprint.pformat()` is saved to a `.py` file, the file is a module that can be imported just like any other.

And since Python scripts are themselves just text files with `.py` file extension, your Python programs can even generate other Python programs. You can then import these files into scripts.
```python
e.g:
>>> import myCats
>>> myCats.cats
[{'name': 'Zophie', 'desc': 'chubby'}, {'name': 'Pooka', 'desc': 'fluffy'}]
>>> myCats.cats[0]
.....
>>> myCats.cats[0]['name']
'Zophie'
```
