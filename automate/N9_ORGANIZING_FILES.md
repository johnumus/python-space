#### Table of contents:
1. [Copying files and folders](#copying-files-and-folders)
2. [Moving and Renaming files and folders](#moving-and-renaming-files-and-folders)
3. [Permanently Deleting files and folders](#permanently-deleting-files-and-folders)
4. [Safe Deletes with the `send2trash` module](#safe-deletes-with-the-send2trash-module)
5. [Walking a directory tree](#walking-a-directory-tree)
    * project based on: rename every file in some folder and also every file in every folder
6. [Compressing files with the `zipfile` module](#compressing-files-with-the-zipfile-module)
    * [Reading ZIP files](#reading-zip-files)
    * [Extracting from ZIP files](#extracting-from-zip-files)
    * [Creating and Adding to ZIP files](#creating-and-adding-to-zip-files)

---

`shutil`(shell utilities) module has functions to let you *copy*, *move*, *rename*, and *delete* files in your Python program.

`import shutil` in required in this section

## Copying files and folders
`shutil.copy(<sauce>, <des>)` : **Returns a string of the path of the copied file**. If `<des>` is a filename, it will be the new name of the copied file.

```js
>>> import shutil
>>> import os

>>> os.chdir('C:\\')

>>> shutil.copy('C:\\spam.txt', 'C:\\delicious')
'C:\\delicious\\spam.txt'

>>> shutil.copy('eggs.txt', 'C:\\delicious\\eggs2.txt')
'C:\\delicious\\eggs2.txt'
``` 

`shutil.copytree(<sauce>, <des>)` : **Returns a string of the copied file's path**. Copy all of its files and subfolders at `<sauce>`. *

```js
>>> import shutil
>>> import os
>>> os.chdir('C:\\')
>>> shutil.copytree('C:\\bacon', 'C:\\bacon_bak')   # inside `bacon` has files and subfolers
'C:\\bacon_bak')
```

## Moving and Renaming files and folders
`shutil.move(<sauce>, <des>)` : **Returns a string of new location** (can be Absolute or Relative depend on the input)

```js
>>> import shutil
>>> shutil.move('C:\\bacon.txt', 'C:\\eggs')
'C:\\eggs\\bacon.txt'   # If `eggs` is a directory
```

:warning: **Warning 1**: If there already had a file with *same name* in `<des>`, it would have been overwritten. 

:warning: **Warning 2**: The folder that make up the `<des>` must exist, or else Python:
* will change the name into it, e.g:
```python
>>> import shutil
>>> shutil.move('testing/testing.py', 'testing/non_existen_folder'   # expect `non_existen_folder`
                                                                     # is exist but it doesn't
'testing/non_existen_folder'                    # testing.py -> non_existen_folder
```
* or throw an exception if the path is long, e.g: `'C:\\does_not_exist\\eggs\ham'` -> raise Exception
```python
.......
>>> shutil.move('testing/testing.py', 'testing/non_existen_folder/non_existen_folder2')
RAISE EXCEPTION
```

## Permanently Deleting files and folders
`os` module : Delete single file or empty folder.

`shutil` module : Delete a folder and all of its contents.

`os.unlink(<path>)` : Delete the file.

`os.rmdir(<path>)` : Delete an empty folder.

`shutil.rmtree(<path>)` : Remove the folder and all of its files and subfolders.
 
:warning: **Warning**: Be careful when using these functions, it's often a good idea to first **commented out** and run `print()` instead.

Below is a Python program with `print()` to safety check before and it protected really well:
```js
>>> import os
>>> for filename in os.listdir():
...     if filename.endswith('.rxt')
...     # os.unlink(filename)
...     print(filename)
```

## Safe Deletes with the `send2trash` module
`pip install send2trash`

```js
>>> import send2trash
>>> baconFile = open('bacon.txt', 'a')  # creates a file
>>> baconFile.write('Bacon is not a vegetable.')
25
>>> baconFile.close()
>>> send2trash.send2trash('bacon.txt')
```
:small_red_triangle_down: **Note**: If you want to free up your disk space use `os` and `shutil`, else use `send2trash`.

## Walking a directory tree 
`os.walk()` : **Return 3 values on each iteration**:
1. A string of current folder's name.
2. A list of strings of the folder(s) in the current folder.
3. A list of strings of the file(s) in the current folder.

```python
>>> import os
>>> for folder_name, subfolders, filenames in os.walk('C:\\delicious'):
...     print('The current folder is ' + folder_name)
...     for subfolder in subfolders:
...         print('Subfolder of ' + folder_name + ': ' + subfolder)
...     for filename in filenames:
...         print('File inside " + folder_name + ': ' + filename)
>>> print('')   # new line
```

## Compressing files with the `zipfile` module
### Reading ZIP files
`zipfile.ZipFile(<zip_file>)` : Create a ZipFile object.

`namelist()` : **Return a list of strings for all files and folders**. These strings can pass to the `getinfo()` ZipFile method to **return ZipInfo object**.

ZipInfo Object have their own attributes like `file_size`, `compress_size`, etc...

While ZipFile Object represent an entire archive file, a ZipInfo object holds useful information about a single file in the archive.
```js
>>> import zipfile,os
>>> os.chdir('C:\\')
>>> exampleZip = zipfile.ZipFile('example.zip')
>>> exampleZip.namelist()
['spam.txt', 'cats/', 'catt/catnames.txt', 'cats/zophie.jpg']
>>> spamInfo = exampleZip.getinfo('spam.txt')
>>> spamInfo.file_size
13980
>>> spamInfo.compress_size
3828
>>> 'Compressed file is %sx smaller!' % (round(spamInfo.file_size / spamInfo.compress_size, 2))
'Compressed file is 3.63x smaller!'
>>> exampleZip.close()
```

### Extracting from ZIP files
`extractall()` : Extract all files and folders from a ZIP files into cwd.

`extract()` :  **Return Absolute or Relative path(depend on your input) to the file was extracted**. Extract a single file from a ZIP file and can extract file into folder we want (if it doesnt exist it will be created). (See the last two example)

:small_red_triangle_down: **Note**: The string pass to `extract()` must match one of the strings in list returned by `namelist()`.
```python
>>> import zipfile, os
>>> os.chdir('C:\\')
>>> exampleZip = zipfile.ZipFile('example.zip')
>>> exampleZip.extractall()
OR> exampleZip.extractall('C:\\extract_folder')     # if `extract_folder` does not
                                                    # exist, it will be created and be extracted to
>>> exampleZip.close()

>>> exampleZip.extract('spam.txt')      # Extract file into cwd
'C:\\spam.txt'

# This example return the Absolute path.
>>> exampleZip.extract('spam.txt', 'C:\\some\\new\\folder')     
'C:\\some\\new\\folders\\spam.txt'

# This example return the Relative path.
>>> exampleZip.extract('spam.txt', '.\\some\\new\\folder')
'some\\new\\folder\spam.txt'

>>> exampleZip.close()
```

###  Creating and Adding to ZIP files
When pass the <path> to `write()` method of ZipFile object, Python will compress file at that path and add it to the ZIP files.

```diff
-Warning:
    'w' : mode will erase all existing contents of ZIP file
    'a' : to add files to an existing ZIP file
```

```js
>>> import zipfile
>>> newZip = zipfile.ZipFile('new.zip', 'w')
>>> newZip.write('spam.txt', compress_type=zipfile.ZIP_DEFLATED)
>>> newZip.close()
```
