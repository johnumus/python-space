

#### :octocat: Table of contents
[**Finding Elements on the page**](#finding-elements-on-the-page)

[**Clicking the page**](#clicking-the-page)

[**Filling Out and Submitting Forms**](#filling-out-and-submitting-forms)

[**Sending Special Keys**](#sending-special-keys)

[**Cliking Browser Buttons**](#clicking-browser-buttons)

---

[**Mouse and Keyboard Control**](#mouse-and-keyboard-control)
 * [**Mouse**](#mouse)
 * [**Image Recognition**](#image-recognition)
 * [**Keyboard**](#keyboard)
 * [Failsafe](#failsafe)

# Finding Elements on the page

| Method Name                                                                                          | WebElement Object/list return                           |
|------------------------------------------------------------------------------------------------------|---------------------------------------------------------|
| `browser.find_element_by_class_name(name)` `browser.find_elements_by_class_name(name)`               | Elements that use the CSS class name                    |
| `browser.find_element_by_css_selector(selector)` `browser.find_elements_by_css_selector(selector)`   | Elements that match the CSS selector                    |
| `browser.find_element_by_id(id)` `browser.find_elements_by_id(id)`                                   | Elements with a matching id attribute value             |
| `browser.find_element_by_link_text(text)` `browser.find_elements_by_link_text(text)`                 | \<a\> elements that completely match the text provided  |
| `browser.find_element_by_partial_link_text(text)` `browser.find_elements_by_partial_link_text(text)` | \<a\> elements that contain the text provided           |
| `browser.find_element_by_name(name)` `browser.find_elements_by_name(name)`                           | Elements with a matching name attribute value           |
| `browser.find_element_by_tag_name(name)` `browser.find_elements_by_tag_name(name)`                   | Elements with a matching tag name (case insensitive; an |

:small_red_triangle_down: **Note:** Except for the `*_by_tag_name()`, the arguments are case sensitive. 

:small_red_triangle_down: **Note:** `selenium` module raises a `NoSuchElement` Exception. If we dont want this shitty, add `try` and `except`. 

| Addtribute or Method  | Description                                                                                             |
|-----------------------|---------------------------------------------------------------------------------------------------------|
| `tag_name`            | The tag name, such as 'a' for an                                                                        |
| `get_attribute(name)` | The value for the element’s name attribute                                                              |
| `text`                | The text within the element, such as 'hello' in hello                                                   |
| `clear()`             | For text field or text area elements, clears the text typed into it                                     |
| `is_displayed()`      | Returns True if the element is visible; otherwise returns False                                         |
| `is_enabled()`        | For input elements, returns True if the element is enabled; otherwise returns False                     |
| `is_selected()`       | For checkbox or radio button elements, returns True if the element is selected; otherwise returns False |
| `location`            | A dictionary with keys 'x' and 'y' for the position of the element in the page                          |

# Clicking the page

```python
>>> from selenium import webdriver
>>> browser = webdriver.Chrome()
>>> browser.get('http://inventwithpython.com')
>>> link_elem = browser.find_element_by_link_text('Read Online for Free')
>>> link_elem.click()
```

# Filling Out and Submitting Forms

```python
>>> from selenium import webdriver
>>> browser = webdriver.Chrome()
>>> browser.get('http://inventwithpython.com')
>>> email_elem = browser.find_element_by_id('login-username')
>>> email_elem.sendkeys('not_my_real_email')
>>\ email_elem.submit()

>>( next_button = browser.find_element_by_id('login-signin')
>>( next_button.click() 

>>> pswd_elem = browser.find_element_by_id('login-passwd')
>>> pswd_elem.sendkeys('1234notexist')
>>> pwed_elem.submit()
```

# Sending Special Keys

| Attributes                                                | Meanings                                      |
|-----------------------------------------------------------|-----------------------------------------------|
| `Keys.DOWN`, `Keys.UP`, `Keys.LEFT`, `Keys.RIGHT`         | The keyboard arrow keys                       |
| `Keys.ENTER`, `Keys.RETURN`                               | The ENTER and RETURN keys                     |
| `Keys.HOME`, `Keys.END`, `Keys.PAGE_DOWN`, `Keys.PAGE_UP` | The home, end, pagedown, and pageup keys      |
| `Keys.ESCAPE`, `Keys.BACK_SPACE`, `Keys.DELETE`           | The ESC, BACKSPACE, and DELETE keys           |
| `Keys.F1`, `Keys.F2`,..., `Keys.F12`                      | The F1 to F12 keys at the top of the keyboard |
| `Keys.TAB`                                                | The TAB key                                   |

```python
>>> from selenium import webdriver
>>> from selenium.webdriver.common.keys import Keys
>>> browser = webdriver.Chrome()
>>> browser.get('url')
>>> html_elem = browser.find_element_by_tag_name('html')
>>> html_elem.send_keys(Keys.END)
>>> html_elem.send_keys(Keys.HOME)
```
Calling `html` tag is a good place to send keys to the general web page. This would be useful because new content is loaded once scrolled to the bottom.




# Clicking Browser Buttons
| Method              | Meaning                         |
|---------------------|---------------------------------|
| `browser.back()`    | Clicks the Back button          |
| `browser.forward()` | Clicks the Forward button       |
| `browser.refresh()` | Clicks the Refresh/Reload butto |
| `browser.quit()`    | Clicks the Close Window button  |


---


# Mouse and Keyboard Control
`pyautogui` and bunch friends of it.
> Automating tests for non-browser apps.
> Automating non-HTML parts of browser apps.
> Cheating at Flash Games.
## Mouse
`click()`

`click([x, y])`

`doubleClick()`

`rightClick()`

`moveTo(x, y[,duration=seconds])`

`moveRel(x_offset, y_offset[,duration=seconds])`

`dragTo(x,y[,duration=seconds])`

---

`position()` - Returns (x, y) tuple.

`size()` - Returns (width, height) tuple.

`displayMousePosition()`

## Image Recognition
| Method                          | Meaning                                             |
|---------------------------------|-----------------------------------------------------|
| `pixel(x, y)`                   | Returns RGB tuple                                   |
| `screenshot()`                  | Returns PIL/Pillow image Object [and saves to file] |
| `locateOnScreen(imageFilename)` | Returns (left, top, width, height) or None          |

```python
>>> import pyautogui
>>> pyautogui.locateOnScreen('7key.png')
>>> pyautogui.locateCenterOnScreen('7key.png')
(TAKE THIS OUTPUT)
>>> pyautogui.click((1421, 331))

```


## Keyboard
`typewrite('<text>'[,interval=secs])`

`press('pageup')`

`pyautogui.KEYBOARD_KEYS`

`hotkey('ctrl', 'o')`

## Failsafe
