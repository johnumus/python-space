#!/usr/bin/env python3
#
# web_scraping.py - 

import bs4
from urllib.request import urlopen
from bs4 import BeautifulSoup as bsoup


url = 'https://www.newegg.com/Product/ProductList.aspx?Submit=Property&N=100007709%2050001402%2050001312%2050001315%2050001561%2050001314%2050001419%2050012150%2050001471%2050001944%20600487565%20600487564%20600565503%20600582123%20600565502%204814%20601273503%20601273511%20601296396%20601296397%2050001669&IsNodeId=1&cm_sp=Cat_video-Cards_2-_-Visnav-_-Performance_1'

# Open a .csv file to save data.
filename = '/home/b0xf/Git/_python-space/product.csv'
f = open(filename, 'w')

headers = 'Brand, Product Name, Price, Shipping\n'

f.write(headers)


# Open to read and store to `page_html`.
u_client = urlopen(url)
page_html = u_client.read()
u_client.close()


# Parse HTML.
soup_file = bsoup(page_html, 'html.parser')
containers = soup_file.findAll('div', {'class':'item-container'})
#container = containers[0]   # take 1 element to analyze


# Save information.
for container in containers: 
    item_brand = container.findAll('a', {'class':'item-brand'})
    item_brand = item_brand[0].img['title']

    product_name = container.a.img['title']

    price_current = container.findAll('li', {'class':'price-current'})
    price_current = price_current[0].strong.text
    price_current_num = container.findAll('a', {'class':'price-current-num'})
    # Because some of it doesn't have Offer:
    if (len(price_current_num) != 0):
        price_current_num = price_current_num[0].text
        price_current += ' ' + price_current_num 

    price_ship = container.findAll('li', {'class':'price-ship'})
    price_ship = price_ship[0].text.strip()

    f.write(item_brand.replace(',', ' ') + ',' + product_name.replace(',', '|') + ',' \
            + price_current + ',' + price_ship + '\n')

f.close()

