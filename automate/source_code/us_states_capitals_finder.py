#!/usr/bin/env python3
#
# us_states_capitals_finder.py - The tool to grab US States Capitals on to clipboard.

import pyperclip
import re


# capitals_regex 
capitals_regex = re.compile(r'''(
        (\w+\s)?                # If State has 2 words name
        (\w+)                   # State's name
        (\s\(\w\w\))            # Abbr of State's name
        (\s-\s)                 # Separator
        (\w+)                   # Capital's name
        (\.)?                   # If Capital's name has a dot
        (\s\w+)?                # If Capital has 2 words name
        (\s\w+)?                # If Capital has 3 words name
        )''', re.VERBOSE)


# Find matches in clipboard text.
text = str(pyperclip.paste())
matches = []

for groups in capitals_regex.findall(text):
    capital_name = "'" + groups[1] + groups[2] + "': '" \
                       + groups[5] + groups[6] + groups[7] + groups[8] + "'"
    matches.append(capital_name)


# Copy results to the clipboard.
if len(matches) > 0:
    text = ''     # clear text to store 

    for i in range(len(matches)):
        if i % 2 == 0:          # after 2 state's capital take a new line
            if (len(matches[0]) > len(matches[i])):
                text += matches[i] + ',\t\t\t'
            else:
                text += matches[i] + ',\t\t'
        else:
            text += matches[i] + ',\n'

    pyperclip.copy(text)
else:
    print('Nothing was found!')


#print('\n'.join(matches))

