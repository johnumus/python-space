#### Table of contents
1. [Raising Exception](#raising-exception)
2. [Getting the traceback as a string](#getting-the-traceback-as-a-string)
3. [Assertions](#assertions)
    * [Using an Assertion in a Traffic Light Simulator](#using-an-assertion-in-a-traffic-light-simulator)
4. [Logging](#logging)
    * [Using `logging` module](#using-logging-module)
    * [Dont debug with `print()`](#dont-debug-with-print)
    * [Logging Levels](#logging-levels)
    * [Disabling Logging](#disabling-logging)
    * [Logging to a file](#logging-to-a-file)




## Raising Exceptions
**If there is no `try` and `except` statements the program will crashes if Exception is raised**. 

`....` 

## Getting the traceback as a string
> When Python encounters an error, it produces a treasure trove of error information called the traceback. The traceback includes the error message, the line number of the line that caused the error, and the sequence of the function calls that led to the error. This sequence of calls is called the call stack.

The traceback is displayed by Python whenever a raised exception goes unhandled. But we can also obtain it as a string by using the function below:

`traceback.format_exc()` : This funciton is useful when we want the information from the traceback and also want an `except` statement gracefully handle the Exception.

Instead of crashing program right after an Exception occur, we can write the traceback information to a log file and keep the program running. (see the example below)
```python
>>> import traceback
>>> try:
...     raise Exception('This is the error message.')
>>> except:
...     error_file = open('error_info.txt', 'w')
...     error_file.write(traceback.format_exc())
...     error_file.close()
...     print('The traceback info was written to error_info.txt')
```

## Assertions
An assertion is a sanity check to make sure our code isn't doing something obviously wrong, if sanity check fails, then an AssertionError Exception is raised.

If an `assert` check is True the program continue to run else it raise Assertion Error.

```js
>>> podBayDoorStatus = 'open'
>>> assert podBayDoorStatus == 'open', 'The pod bay doors need to be "open" !'

>>> podBayDoorStatus = 'Im sorry i cant open it'
>>> assert podBayDoorStatus == 'open', 'The pod bay doors need to be "open" !'
RAISE TRACEBACK -> AssertionError
```
> In plain English, assert statements says "I assert that this condition holds true, if not there is some bug somewhere in the program".

```diff
+ By falling fast like this, your shorten the time between the original cause of the bug 
+ and when you first notice the bug, this will reduce the amount of code
+ you will have to check before finding the code that's causing the bug.
```

:small_red_triangle_down: **Note**: Assertions are for Programmer Errors, not user errors.

### Using an Assertion in a Traffic Light Simulation
The program below is a wrong program just show how `assert` handle. 

It could take hours to trace the bug back if we didnt use `assert`.

We know that **at least one of the lights is always red**, so include `assert` to see what will happen:
```python
market_2nd = {'ns': 'green', 'ew': 'red'}
mission_16th = {'ns': 'red', 'ns': 'green'}

def switch_lights(stoplight):
    for key in stoplight.keys():
        if stoplight[key] == 'green':
            stoplight = 'yellow'
        elif stoplight[key] == 'yellow':
            stoplight = 'red'
        elif stoplight[key] == 'red':
            stoplight = 'green'

        assert 'red' in stoplight.values(), 'Neither light is red! ' + str(stoplight)
```

### Disabling Assertions
Passing `-O` option when running Python.

> It "could" make your program slowed down (although most of the time `assert` statements do not cause a noticeable speed difference)

> Assertions is for Development, not the final product. By the time your hand off your program to someone else to run, it should be free of bugs and not require the sanity checks.

## Logging
```python
import logging
logging.basicConfig(level=logging.DEBUG, format=' %(asctime)s - %(levelname)s - %(message)s')
```
### Using `logging` module
Basically, when Python logs an event, it creates a LogRecord Object that holds information about that event.

`logging.basicConfig()` : Specific what details about LogRecord Object you want to see and how it displayed.

```python
import logging

# specific it, so when print out not just the string passed to them, but also a timestamp and the world "DEBUG".
# eg: 2019-01-10 16:09:50,xxx - DEBUG - Start of program
logging.basicConfig(level=logging.DEBUG, format' %(asctime)s - %(levelname)s - %(message)s')

def factorial(n):
    logging.debug('Start a program')

    total = 1
    for i in range(1, n+1):
        total *= 1
        logging.debug('i is' + str(i) + ', total is ' + str(total))
    logging.debug('End of factorial(%s)' %(n))

    return total

print(factorial(5))
logging.debug('End of program')
#PRINT-OUT:
...........................
...........................
...........................
120
..........................
```

### Dont debug with `print()`

### Logging Levels
|    Level   | Logging Function   | Description                                                                                                     |
|:----------:|--------------------|-----------------------------------------------------------------------------------------------------------------|
| DEBUG      | logging.debug()    | Used for small details. Usually we only care about this when diagnosing problems.                               |
| INFO       | logging.info()     | Used to record information on general events in your program or confirm that things are working at their point. |
| WARNING    | logging.warning()  | Used to indicate a potential problem may cause in the future.                                                   |
| ERROR      | logging.ERROR()    | Used to record an error that caused the program to failed to do something.                                      |
| CRITICAL() | logging.CRITICAL() | Used to indicate a fatal error that has caused or is about to caused the program to stop running entirely.      |

And the priority is from the highest down, pass `WARNING` to `logging.basicConfig()`'s `level` argument like:

`logging.basicConfig(level=logging.WARNING, ....)` : This will only show WARNING, ERROR and CRITICALmessages.

```python
>>> import logging
>>> logging.basicConfig(level=logging.DEBUG, format=' %(asctimes)s - %(levelname)s - %(message)s')
>>> logging.debug('Some debugging details.')
(timestamp) - DEBUG - Some debugging details.
>>> logging.info('The logging module is working.')
......
>>> logging.warning('An error message is about to be logged.')
......
>>> logging.error('An error has occurred.')
.....
>>> logging.critical('The program is unable to recover.')
.....
```

### Disabling Logging
Like the Logging Levels above, if we want to disable logging entirely, just add `logging.disable(logging.CRITICAL)`. 

This will disable all messages after it, so it's a tip that we should add it after `import logging` to easy to find and comment it out.

```python
import logging

logging.disable(logging.CRITICAL)

.......
```

### Logging to a file
It keeps your screen clear and store the messages so we can read them later.

```python
import logging
logging.basicConfig(filename='<file_name>.txt', level=logging.DEBUG, format='%s(asctime)s - %(levelname)s - %(message)s')
```

