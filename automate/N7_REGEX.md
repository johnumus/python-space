#### Table of contents:
1. [Grouping with the Parenthese&nbsp;&nbsp;&nbsp; **()**](#grouping-with-the-parenthese)
2. [Matching Multiple Groups with the Pipe&nbsp;&nbsp;&nbsp; **|**](#matching-multiple-groups-with-the-pipe)
3. [Optional matching with the Question Mark&nbsp;&nbsp;&nbsp; **?**](#optional-matching-with-the-question-mark)
4. [Matching Zero or More with the Star&nbsp;&nbsp;&nbsp; __*__](#matching-zero-or-more-with-the-star)
5. [Matching One or More with the Plus&nbsp;&nbsp;&nbsp; **+**](#matching-one-or-more-with-the-plus)
6. [Matching Specific Repetition with Curly Brackets &nbsp;&nbsp;&nbsp;**{}**](#matching-specific-repetition-with-curly-brackets)
* [Greedy and Non-Greedy](#greedy-and-non-greedy)
7. [Character Classes](#character-classes)
* [Making your own Character Classes](#making-your-own-character-classes)
8. [The Caret and Dollar Sign character&nbsp;&nbsp;&nbsp;**^** **$**](#the-caret-and-dollar-sign-character)
9. [The Wildcard character&nbsp;&nbsp;&nbsp;**.**](#the-wildcard-character)
10. [Matching Everything with Dot-star&nbsp;&nbsp;&nbsp;__.*__](#matching-everything-with-dot-star) 
11. [Matching Newlines with the Dot character&nbsp;&nbsp;&nbsp;**re.DOTALL**](#matching-newlines-with-the-dot-character)
12. [Case-Insensitive matching&nbsp;&nbsp;&nbsp;**re.I**](#case-insensitive-matching)
13. [Substituting strings with the **sub()** method](#substituting-strings)
14. [Managing Complex Regexes with&nbsp;&nbsp;&nbsp;**re.VERBOSE**](#managing-complex-regexes) 
15. [Combining **re.IGNORECASE**, **re.DOTALL**, **re.VERBOSE**](#combining)

# Summary
    () - Grouping

    |  - Either or 


## Grouping with the Parenthese 
  >  **()**
  ```python
  r'(\d{3})-(\d{3}-\d{4})'
  ```
  Passing number to `group(<number>)`
  * 1 for group 1
  * 2 for group 2
  * 0 for the entire

  \*Trick: Retrive all the group at once, use `groups()`(the "s" different)
  ```python
  groups() # return a Tuple of multiple values
  
  # e.g.
  >>> mo.groups()
  ('415', '555-4242')
  >>> area_code, main_number = mo.groups()
  >>> print(area_code)
  415
  >>> print(main_number)
  555-4242
  ```
  

## Matching Multiple Groups with the Pipe
>  **|**
```python
r'Batman|Tina Fey'
r'Bat(man|mobile|copter|bat)'
```
`search()` : Find the only first one (returns a "match object")

`findall()` : Find all (returns "list of strings" if no groups ;else returns "list of tuples"



## Optional matching with the Question Mark

>  **?**
```python
r'Bat(wo)?man'
r'(\d{3})?\d{3}-d{4}'

# e.g.
>>> phoneRegex = re.compile(r'(\d{3})?\d{3}-\d{4})
>>> mo1 = phoneRegex.search('My number is 412-442-8621')
>>> mo1.group()
'412-442-8621'

>>> mo2 = phoneRegex.search('My number is 442-8621')
>>> mo2.group()
'442-8621'
```



## Matching Zero or More with the Star
>  __*__ 
```python
r'Bat(wo)*man'

# e.g.
>>> batRegex = re.compile(r'Bat(wo)*man')
>>> mo1 = batRegex.search('The Advanture of Batman')
>>> mo1.group()
'Batman'

>>> mo2 = batRegex.search('Batwoman')
>>> mo2.group()
'Batwoman'

>>> mo3 = batRegex.search('Batwowowoman')
'Batwowowoman'
```



## Matching One or More with the Plus
> **+**
```python
r'Bat(wo)+man'

# e.g.
# Similar to the above but it required at least ONE match
```



## Matching Specific Repetition with Curly Brackets
> **{}**
```python
r'(Ha){3}'
# e.g. (will match only)
'HaHaHa'

r'(Ha){3:5}'
# e.g. (will match from 3 -> 5)
'HaHaHa'
'HaHaHaHa'
'HaHaHaHaHa'

r'(Ha){3,}' # match 3 or more

r'(Ha){,5}' # match zero to 5 
```
### GREEDY and NON-GREEDY
```python
# GREEDY
>>> re.compile(r'(Ha){3,5}')
>>> .search('HaHaHaHaHa')
'HaHaHaHaHa'

# NON-GREEDY (note the "?")
>>> re.compile(r'(Ha){3,5}?') 
'HaHaHa'
```



## Character Classes

    \d    - numeric digit from 0-9
    \D    - opposite

    \w    - letter, digit, underscore
    \W    - opposite

    \s    - space, tab, newline
    \S    - opposite
    # e.g.
         [0-5] shorter than typing (0|1|2|3|4|5)


### Making your own character classes
```python
r'[<character>]
r'[a-zA-Z0-9] # include ranges of ....

# Negative character class:
r'[^<character>] # note the "^"
```
\*Note: Inside the square brackets, the normal regex symbols are not interpreted as such: . \* ? () &nbsp;(No need to escape)


## The Caret and Dollar Sign character
> **^** **$**

**^** : The match must occur at the beginning

**$** : Opposite

```java
# e.g.
>>> re.compile(r'^(h|H)ello')
>>> .search('<text_below>')
'Hello world!'              => match='Hello'
'The world says hello'      -> None

>>> re.compile(r'\d$')
>>> .search('<text_below>')
'Your lucky number is 42'   => match='2'
'42 hoe'                    -> None
```


## The Wildcard character
> **.**
Matching any single character except for a newline
```python
>>> ...'.at'
>>> .findall('The cat in the hat sat on the flat mat.')
['cat', 'hat', 'sat', 'lat', 'mat']
```


## Matching Everything with Dot-Star
> __.*__
    explained:
      `.` : any single character except for a newline
      `*` : zero or more 
      So combine 2 of them
```python
>>> r'First Name: (.*) Last Name: (.*)'
>>> .search('First Name: Al Last Name: Fuck')
>>> .group(1)
'Al'
>>> .group(2)
'Fuck'

# Non-greedy:
>>> nonRegex = re.compile(r'<.*?>')
>>> .search('<To serve man> for dinner.>')
>>> .group()
'<To serve man>'

# Greedy:
>>> ...(r'<.*>)
>>> .search('SIMILAR_TO_THE_ABOVE')
>>> .group()
'<To serve man> for dinner.>'
```


## Matching Newlines with the Dot character
> **re.DOTALL**

```python
# Dont have
>>> re.compile('.*')
>>> .search('Serve the public trust.\nProtect the innocent.\Uphold the law.').group()
'Serve the public trust'

# Do have
>>> re.compile('.*', re.DOTALL)
>>> .search('SIMILAR THE ABOVE')
'Serve the public trust.\nProtect the innocent.\nUphold the law.'
```

## Case-Insensitive matching
> **re.I**
```java
(r'robocop', re.I)
>>>.search(<text_below>)
'Robocop' -> match
'RoBOCoP' -> match
```

## Substituting strings
> **sub()**
```python
# e.g.
>>> namesRegex = re.compile(r'Agent \w+')
>>> namesRegex.sub('CENSORED', 'Agent Alice gave the secret to Agent Bob.')
'CENSORED gave the secret to CENSORED.' 

>>> (r'Agent \w+')
>>>regexObj.sub(r'\1****', 'Agent Alice to Agent Carol that Agent Eve knew Agent Bob was a double agent.')
'A**** told C**** that E**** knew B**** was a double agent.' 
```

## Managing Complex Regexes
> **re.VERBOSE**
```java
# e.g.
phoneRegex = re.compile(r'((\d{3}|\(\d{3}\))?(\s|-|\.)?\d{3}(\s|-|\.)\d{4}
			                (\s*(ext|x|ext.)\s*\d{2,5})?)')
-> 
phoneRegex = re.compile(r'''(
(\d{3}|\(\d{3}\))?          # area code
(\s|-|\.)?                  # separator
\d{3}                       # first 3 digits
(\s|-|\.)?                  # separator
\d{4}                       # last 4 digits
(\s*(ext|x|ext.)\s*\d{2,4})?# externsion
)''', re.VERBOSE)
```

## Combining
> Combining **re.I**, **re.DOTALL**, **re.VERBOSE**

    <place_holde>
