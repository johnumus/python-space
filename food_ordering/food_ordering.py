class Foodstuff:
    
    def __init__(self, name=None, s_price=None, m_price=None, l_price=None):
        self.name = name
        self.s_price = s_price
        self.m_price = m_price
        self.l_price = l_price

    @property 
    def size_and_price(self):
        return {'s': self.s_price, 'm': self.m_price, 'l':self.l_price}

    def __str__(self):
        return '{0}\n \tSmall: ${1:.2f}\tMedium: ${2:.2f}\tLarge: ${3:.2f}' \
                .format(self.name, self.s_price, self.m_price, self.l_price)
    


class Food(Foodstuff):
    # f_price :: family price
    def __init__(self, name, s_price, m_price, l_price, f_price=None):
        super().__init__(name, s_price, m_price, l_price)
        self.f_price = f_price

    def __str__(self):
        if self.f_price is not None:
            return super().__str__() + '\t Family: ${:.2f}'.format(self.f_price)
        return super().__str__()



class Drink(Foodstuff):
    def __init__(self, name, s_price, m_price=None, l_price=None):
        super().__init__(name, s_price, m_price, l_price)

    def __str__(self):
        if self.s_price is not None and self.m_price is None:
            return '{0}\n \tBottle: ${1:.2f}'.format(self.name, self.s_price)
        return super().__str__()












if __name__ == '__main__':
    chicken = Food('Chicken', 4.99, 10.99, 13.99)
    hamburger = Food('Hamburger', 2.49, 4.49, 7.99)
    pizza = Food('Pizza', 5.29, 11.99, 16.99, 19.99)

    print('\t\t\t< Price in (USD) >')
    print(chicken)
    print(hamburger)
    print(pizza)

    pepsi = Drink('Pepsi', 0.99, 1.19, 1.39)
    orange_juice = Drink('Orange Juice', 1.79, 2.09, 2.49)
    sweet_tea = Drink('Sweet Tea', 1.00, 1.19, 1.39)
    iced_coffee = Drink('Iced Coffee', 0.99, 1.39, 1.89) 
    water = Drink('Aquafina Water', 1.00)

    print('-----------------------------------------------------')
    print(pepsi)
    print(orange_juice)
    print(sweet_tea)
    print(iced_coffee)
    print(water)

