class Employee:

    raise_pay = 1.00

    def __init__(self, first_name, last_name, seniority):
        self.first_name = first_name
        self.last_name = last_name
        self.seniority = seniority

    
    def __str__(self):
        return 'First Name: {0}\nLast Name: {1}\nSeniorty: {2:d} month(s)' \
                .format(self.first_name, self.last_name, self.seniority)


class Manager(Employee):
    '''
    Raise pay of Manager when reach 6 months: 5%
                                    1 year  : 7% 
                                    2 years : 10% maximum
    '''
    def __init__(self, first_name, last_name, seniority):
        super().__init__(first_name, last_name, seniority)

    def __str__(self):
        return super().__str__() + '\n' + 'Position: Manager\n'

class Developer(Employee):
    '''
    Raise pay of Developer when reach 6 months: 5%
                                      1 year  : 7%
                                      2 years : 10% maximum
    '''
    def __init(self, first_name, last_name, seniority):
        super().__init__(first_name, last_name, seniority)

    def __str__(self):
        return super().__str__() + '\n' + 'Position: Developer\n'

class Cook(Employee):
    '''
    Raise pay of Cook when reach 3 months: 3%
                                 6 months: 6% maximum
    '''
    def __init__(self, first_name, last_name, seniority):
        super().__init__(first_name, last_name, seniority)

    def __str__(self):
        return super().__str__() + '\n' + 'Position: Cook\n'

class CrewMember(Employee):
    '''
    Raise pay of CrewMember  when reach 3 months: 2%
                                        6 months: 5%
                                        1 year is promoted to CrewTrainer

    Raise pay of CrewTrainer when reach 6 months: 7%
                                        1 year: 10% maximum 
    '''
    def __init__(self, first_name, last_name, seniority, is_trainer=None):
        super().__init__(first_name, last_name, seniority)
        self.is_trainer = is_trainer
    
    def __str__(self):
        if self.is_trainer is None:
            return super().__str__() + '\n' + 'Position: Crew Member\n'
        else:
            return super().__str__() + '\n' + 'Position: Crew Trainer\n'

if __name__ == '__main__':
    emp = Employee('Tiy', 'Bunny', 2)
    print(emp)
    print()

    crew = CrewMember('Tiny', 'Bitch', 13, None)
    print(crew)

    cook = Cook('Alain', 'Wilson', 24)
    print(cook)

    manager = Manager('Justin', 'K. Hall', 2)
    print(manager)

    dev = Developer('Elliot', 'Anderson', 24)
    print(dev)
